package controllers;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import models.Data;
import models.User;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class UserController {

    public UserController() {
        RestAssured.baseURI = "https://reqres.in/api";
 }

    public Response getAllUsers() {
        return  given().
                when().
                get("/users?page=2").
                then().
                assertThat().statusCode(200).and().
                contentType(ContentType.JSON).and().
                header("server", "cloudflare").and().
                body("page", equalTo(2)).and().
                extract().
                response();
    }

    public User getUserById(String userId) {
        return given()
                .when()
                .get("/users/" + userId)
                .getBody()
                .as(Data.class)
                .getUser();
    }

    public Response deleteUser(String userId) {
        return given().
                when().
                delete(String.format("/users/%s",userId)).
                then().assertThat().
                statusCode(204).and().
                header("server", "cloudflare").and().
                extract().
                response();
    }

    public Response createUser(String id,String name, String job) {
        String bo = String.format(
                " '   {  '  + \r\n" +
                        " '       \"id\": \"%s\",  '  + \r\n" +
                        " '       \"name\": \"%s\",  '  + \r\n" +
                        " '       \"job\": \"%s\"  '  + \r\n" +
                        " '  }  ' ; ", name, job,id);
        return given().
                body("bo").
                when().
                post("/api/users").
                then().assertThat().
                statusCode(201).and().
                contentType(ContentType.JSON).and().
                header("server", "cloudflare").and().
                header("content-length", "51").
                extract().
                response();
    }
}
