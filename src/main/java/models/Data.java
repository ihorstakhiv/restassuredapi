package models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Data {
    @JsonProperty("data")
    User user;

    public Data(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Data{" +
                "user=" + user +
                '}';
    }

    public Data() {
    }
}
