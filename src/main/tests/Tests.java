import controllers.UserController;
import models.User;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Tests {
    private UserController userController;

        @BeforeClass(alwaysRun = true)
        public void setUp() {
            userController = new UserController();
        }

        @Test
    public void verifyDeleteUser(){
            userController.createUser("22","Ihor", "22");
            User userAfterCreating = userController.getUserById("22");
            userController.deleteUser("22");
            User userAfterDeleting = userController.getUserById("22");
            Assert.assertNotEquals(userAfterCreating,userAfterDeleting);
        }

}
